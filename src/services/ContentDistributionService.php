<?php
/**
 * Created by PhpStorm.
 * User: nicolaskuster
 * Date: 2019-02-28
 * Time: 08:24
 */

namespace Quint\ContentDistribution\services;


use Craft;
use yii\base\Component;

class ContentDistributionService extends Component
{
    public function renderEntrySidebar(&$context)
    {
        //@todo check if entry is Saved
        switch ($context['entry']->sectionId) {
            case 4:
                return Craft::$app->view->renderTemplate('content-distribution/sidebarpanel_event', ['context' => $context]);
                break;
            case 3:
            case 15:
                return Craft::$app->view->renderTemplate('content-distribution/sidebarpanel_teaser', ['context' => $context]);
                break;
            default:
                return null;
        }
    }
}