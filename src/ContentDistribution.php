<?php
/**
 * Created by PhpStorm.
 * User: nicolaskuster
 * Date: 2019-02-28
 * Time: 08:16
 */

namespace Quint\ContentDistribution;


use Craft;
use craft\events\RegisterUrlRulesEvent;
use craft\web\UrlManager;
use Quint\ContentDistribution\services\ContentDistributionService;
use yii\base\Event;

class ContentDistribution extends \craft\base\Plugin
{

    //Muss auf true gesetzt werden, damit das in der Userverwaltung
    //die Berechtigungen für dieses plugin gesetzt werden können
    //Der Eintrag wird über getCPNavItem wieder ausgeblendet
    public $hasCpSection = true;

    public function init()
    {
        parent::init();
        $this->setPluginComponents();
        $this->registerCpRoutes();

        Craft::$app->view->hook('cp.entries.edit.details', [$this->getService(), 'renderEntrySidebar']);
    }

    /**
     * {@inheritdoc}
     */
    public function getCpNavItem()
    {
        //Eintrag welcher mit $hasCpSection = true gesetzt wurde wieder ausblenden
        return;
    }

    private function setPluginComponents()
    {
        $this->setComponents([
            'service' => ContentDistributionService::class
        ]);
    }

    private function registerCpRoutes()
    {
        Event::on(UrlManager::class, UrlManager::EVENT_REGISTER_CP_URL_RULES, function (RegisterUrlRulesEvent $event) {
            $event->rules = array_merge($event->rules, [
                'content-distribution/send' => 'content-distribution/content-distribution/send',
            ]);
        });
    }

    public function getService()
    {
        return $this->get('service');
    }
}