<?php
/**
 * Created by PhpStorm.
 * User: nicolaskuster
 * Date: 2019-02-28
 * Time: 10:14
 */

namespace Quint\ContentDistribution\controllers;


use Craft;
use craft\elements\db\EntryQuery;
use craft\elements\Entry;
use craft\services\Assets;
use craft\web\Controller;
use craft\web\View;

class ContentDistributionController extends Controller
{
    public function actionSend()
    {
        $this->requirePostRequest();

        $bodyParams = Craft::$app->request->bodyParams;
        $entryId = $bodyParams['entryId'];

        $entry = Craft::$app->entries->getEntryById($entryId);

        switch ($entry->section->id) {
            case 4:
                $this->sendEvent($entry);
                break;
            case 3:
            case 15:
                $this->sendTeaser($entry);
        }
    }

    protected function sendMail($recipients, $subject, $messageBody)
    {
        $recipients = array_map(function ($entry) {
            return $entry->mail;
        }, $recipients);

        $mail = Craft::$app->getMailer()->compose();
        $mail->setBcc($recipients);
        $mail->setSubject($subject);
        $mail->setHtmlBody($messageBody);
        $mail->send();
    }

    protected function sendTeaser($entry)
    {
        Craft::$app->getView()->setTemplateMode(View::TEMPLATE_MODE_SITE);

        if ($entry->metaImage->one()) {
            try {
                $metaImageUrl = Craft::$app->assets->getAssetUrl($entry->metaImage->one(), 'maxSizeTransformation', true);
            } catch (\Exception $exception) {
                $metaImageUrl = $entry->metaImage->one()->getUrl();
            }
        } else {
            $metaImageUrl = null;
        }

        $messageBody = Craft::$app->view->renderTemplate('_emails/content-distribution-message', ['entry' => $entry, 'metaImageUrl' => $metaImageUrl]);


        $recipients = (new EntryQuery(new \craft\elements\Entry()))
            ->section('teaserRecipients')
            ->all();

        $this->sendMail($recipients, 'WILWEST: Neuer Artikel zur Publikation verfügbar', $messageBody);
    }

    protected function sendEvent($entry)
    {
        Craft::$app->getView()->setTemplateMode(View::TEMPLATE_MODE_SITE);

        $messageBody = Craft::$app->view->renderTemplate('_emails/event-distribution-message', ['entry' => $entry]);

        $recipients = (new EntryQuery(new \craft\elements\Entry()))
            ->section('eventRecipients')
            ->all();

        $this->sendMail($recipients, $entry->title, $messageBody);
    }
}